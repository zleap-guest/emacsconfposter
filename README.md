# EmacsConfPoster

Poster for EmacsConf 2020

- **ConTeXt version**
  - [PDF](ConTeXt_version/poster_emacs_v02.pdf)
  - [ConTeXt source](ConTeXt_version/poster_emacs_v02.tex)
  - the TeX file inclued a MetaPost version of the emacs logo used for EmacsConf

Dates and content have to be updated when available.
